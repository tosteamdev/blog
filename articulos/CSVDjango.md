# Sirviendo archivos CSV con Django en 5 minutos

En ocasiones te enfrentas a la tarea de diseñar e implementar funciones que permitan al usuario de tus aplicación descargar sus datos en archivos a fin de que ellos puedan conservarlos y poder consultarlos luego. Con Python y Django puede ser relativamente sencillo a traves de las librerias disponibles. En esta guia presento una alternativa para implementar esta funcion utilizando Python3, Django 1.8 y la libreria Tablib.

*Nota: Para construir las tablas con las que luego escribiremos el archivo CSV utilizaremos la libreria tablib. Esta libreria facilita el manejo y contrucción de tablas a traves de las listas y diccionarios de Python, adicionalmente exporta en distintos formatos que posteriormente pueden ser escritos. Recomiendo visitar su [pagina] [1] para conocer su instalación y uso.


## Construyendo nuestro CSV

El uso de **tablib** es extremadamente sencillo, para inciar se debe importar la libreria que previamente se ha instalado y crear un objeto de la clase Dataset.

    import tablib

    # creamos un objeto
    data = tablib.Dataset()

    # collection of names
    paises = ['Colombia Bogotá', 'Italia Roma', ]

    for pais_item in paises:
        # Separamos los valores
        pais, capital = pais_item.split()
        # Adicionamos a la estructura
        data.append([pais, capital])

    # Definimos un titulo
    data.headers = ['Pais', 'Capital', ]

Con la estructura construida **tablib** permite exportar a cualquier tipo de dato y luego escribirla sobre un archivo.

    # Estructura Diccionario
    data.dict

    # Estructura CSV
    data.csv

## Un mixin para Django

Para responder un archivo CSV con Django modificaremos el HttpResponse para que entregue el archivo al usuario, sin embargo recomiendo personalmente utilizar un Mixin que podrá ser utilizado en cualquier vista.

### Mixin

    class CSVResponseMixin(object):
        def render_to_csv(self, data):
            response = HttpReponse(context_type='text/csv')
            response[
                'Content-Disposition'
            ] = 'attachment; filename=download.csv'
            response.write(data)
            return response

### View

Si nuestras vistas en Django estan basadas en clases puede sobreescribirse el metodo render_to_response() y responder con el mixin previamente importado.

    Class MyView(CSVResponseMixin, View):
        def render_to_response(self, context):
            # Construye tus datos con tablib
            data = self.constuir_datos()
            return self.render_to_csv(data.csv)

En el anterior codigo se puede observar que no utilizamos super() para sobreescribir el metodo render_to_response() sino que al contrario utilizamos el metodo del mixin para resolver la respuesta.

*Nota: Sobre la funcion render_to_csv() se ha pasado por parametro los datos construidos seguido del atributo .csv este atributo es heredado de la clase Dataset(). Dejo a su criterio como manejar el objeto y obtener la representación, sin embargo es importante anotar que sobre el response del Mixin es necesario enviar los datos que seran escritos.

[1]: https://tablib.readthedocs.org/en/latest/
