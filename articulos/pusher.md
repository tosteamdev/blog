# Interaccion Pusher front-end

[Pusher](http://pusher.com) es un servicio que permite generar notificaciones al usuario referente a tareas terminadas en un backend.
Por ende pusher funciona y convive bien en un ambiente asincrono, donde las actividades se generan pero no se esperan sus resultados;
asi este sensillo ejemplo de como pusher ayuda a actualizar elementos visuales al terminar las tareas, funciona bajo un servicio de back-end
usando [Django](https://www.djangoproject.com) y [Celery](http://www.celeryproject.org) para el manejo de tareas asincronas.

## Orden y necesidades

### cssFiles
Para este caso usamos la libreria [materialize](http://materializecss.com) para el manejo de estilos y usar herramientas que brinda,
en especial los dialogs

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
### jsFiles

    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/js/materialize.min.js"></script>
    <script src="//js.pusher.com/2.2/pusher.min.js"></script>
    
### body
Ya que el poryecto es django hacemos uso de la vercion de Jinja integrada

    <body>
    {# logic would be put jinja cycle to render all existing queries in back-end #}
    
      <div class="card-panel grey lighten-5 z-depth-1">
        <div class="row valign-wrapper">
    
          {# element that is rendered by Django an have a computing process to be ready #}
          <div id="Q{{ query.pk }}" class="col s1">
            {% if query.ready %}
            <a class="btn-floating waves waves-teal"
            href="{% url 'seeQueryUrl' query.pk %}">
            <i class="material-icons">play_arrow</i>
          </a>
          {% else %}
          {# this is a spinner for the queries that arent ready yet #}
          <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
              <div class="circle-clipper left">
                <div class="circle"> </div>
              </div>
            </div>
            <div class="spinner-layer spinner-red">
              <div class="circle-clipper left">
                <div class="circle"> </div>
              </div>
            </div>
            <div class="spinner-layer spinner-green">
              <div class="circle-clipper left">
                <div class="circle"> </div>
              </div>
            </div>
          </div>
          {% endif %}
        </div>
        <div class="col s10 offset-s1 m11">
          <span class="black-text truncate">
            {{ query.description }}
          </span>
        </div>
        {# adding if ready a button for download the file genereted from query #}
        {% if query.ready %}
        <a id="fileQ{{ query.pk }}" class="rigth" href="{% url 'downloadFileUrl' query.pk %}">
          <i class="material-icons">file_download</i>
        </a>
        {% endif %}
      </div>
    </div>
    </body>

### script
Finalmente el scrip que permite escuchar que la actividad o tarea ha terminado para dar inicio a un cambio visual para notificar al usuario

    (function () {
        // Ask for pusher details with an ajax query
        var pusher = new Pusher('APP_KEY');
        var channel = pusher.subscribe('MY_CHANEL');
        channel.bind('MY_CHANEL_EVENT', function (data) {
          // when ready change elements by data and notify
    
          // data brings pk of query and url from get the query file
    
          var divtoget = "#Q"+data.pk;
          var file = "#fileQ"+data.pk;
    
          // crating new elements to show when query is ready
          $newhtml=$("<a class='btn-floating waves waves-teal' style='opacity:0;' href='"+data.url
            +"'><i class='material-icons'>play_arrow</i></a>");
    
          $button=$("<a id='fileQ"+data.pk+"' style='opacity:0;' class='rigth' href='"+data.url+"/"+data.pk
            +"'><i class='material-icons'>file_download</i></a>");
    
    
    
          // this only works if in the page exist the element $(divtoget) otherwise will show onli
          // the notification dialog
          $(divtoget).children().fadeOut(500,function(){
            $(divtoget).empty();
            $(divtoget).append($newhtml);
            $(divtoget).children().fadeTo(500,1);
            $(divtoget).parent().append($button)
            $(file).fadeTo(500,1);
    
          });
    
          // materialize dialog notification and btn for go to see the query result
          Materialize.toast("<span>The tree has grown up!</span><a class='btn-flat yellow-text' href='"+data.url+"'><i class='material-icons' style='font-size:30px'>foreward</i><a>", 10000);
        });
    })();